package com.unnamedgame.attack;

import java.util.Random;

public abstract class Attack {
	//Variable Declaration
	private float critical_chance = 0.00F, critical_multiplier = 1.00F, accuracy = 0.8F;
	private byte required_proficiency = 75, attack_delay;
	private int energy_requirement = 1, damage_range = 3;
	//Constructors
	public Attack() {

	}
	public Attack setAccuracy(float a) {
		this.accuracy = a;
		return this;
	}
	public Attack setAttack_delay(byte attack_delay) {
		this.attack_delay = attack_delay;
		return this;
	}
	public Attack setCritical_chance(float c) {
		this.critical_chance = c;
		return this;
	}
	public Attack setCritical_multiplier(float m) {
		this.critical_multiplier = m;
		return this;
	}
	public Attack setDamage_range(int r) {
		this.damage_range = r;
		return this;
	}
	public Attack setEnergy_requirement(int e_r) {
		this.energy_requirement = e_r;
		return this;
	}
	public Attack setRequired_proficiency(byte p) {
		this.required_proficiency = p;
		return this;
	}
	public Attack setDirectEffects(float accuracy, byte speed, short damage_range, short energy_required) {
		this.accuracy = accuracy;
		this.attack_delay = speed;
		this.damage_range = damage_range;
		this.energy_requirement = energy_required;
		return this;
	}
	//Getter Methods
	public float getAccuracy() {
		return this.accuracy;
	}
	public byte getAttack_delay() {
		return attack_delay;
	}
	public float getCritical_chance() {
		return this.critical_chance;
	}
	public float getCritical_multiplier() {
		return this.critical_multiplier;
	}
	public float getDamage_range() {
		return this.damage_range;
	}
	public int getEnergy_requirement() {
		return this.energy_requirement;
	}
	public byte getRequired_proficiency() {
		return this.required_proficiency;
	}
	//Methods
	public boolean isAccurate(float enemy_evasiveness) {
		float random = (float)Math.random();
		if(random <= this.accuracy) {
			return true;
		}
		return false;
	}
	public int attackDamage(short player_weapon_level, int weapon_base_damage) {
		Random newRandom = new Random();
		int base_damage = (short)(2 * player_weapon_level / 3) + weapon_base_damage;
		int modifier = 1 + newRandom.nextInt() % this.damage_range;
		return base_damage + modifier;
	}
}
