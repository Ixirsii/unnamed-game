package com.unnamedgame.world;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.lwjgl.util.vector.Vector2f;

public class DynamicRegion extends Region {
	/**
	 * How big to build the region around the <code>central camera</code>
	 *
	 * @see DynamicRegion#centralCamera
	 */
	private float regionSize;
	/**
	 * If the dynamic region overlaps a static region within the weight, it will
	 * only load the static region.
	 */
	private float weight;
	/**
	 * The total number of polygons being drawn (in this region)
	 */
	private int numberPolygons = 0;
	/**
	 * A set of the regions that overlap and surround the dynamic region for
	 * easier getting of near entities;
	 */
	private List<StaticRegion> overlappingRegions;
	/**
	 * Pointer to the world that this region exists in
	 */
	private World world;
	/**
	 * The dynamic region is built around the <em>central camera</em>
	 */
	private UUID centralEntity;

	/**
	 * Creates a new DynamicRegion centered around the specified entity,
	 * starting at the specified points
	 *
	 * @param entity
	 *            The entity the Region is centered around.
	 * @param world
	 *            The world that this region exists in.
	 */
	public DynamicRegion(String name, UUID entity,
						 World world) {
		super(name);
		this.world = world;
		centralEntity = entity;
		init();
	}

	private void init() {
		regionSize = 50F;
		weight = 10F;
		overlappingRegions = new ArrayList<>();
		update();
	}

	/**
	 * Checks to make sure that all the entities from the overlapped region are
	 * registered, and calls <code>removeExtraEntities</code> if extra entities
	 * are registered.
	 *
	 * @see DynamicRegion#removeExtraEntities()
	 */
	private void getEntitiesFromOverlap() {
		if (overlappingRegions.size() > 1) {
			// error handling
			return;
		}
		Region region = overlappingRegions.get(0);
		for (UUID entity : region.regionEntities) {
			if (!regionEntities.contains(entity)) {
				regionEntities.add(entity);
			}
		}
	}

	/**
	 * Iterates over all the entities in region and if the entity<br>
	 * (a) Has a position<br>
	 * (b) Position is inside this dynamic region<br>
	 * (c) entity is not already registered in this region<br>
	 * the entity will be registered into this dynamic region
	 *
	 * @param region
	 *            The region to check entities in.
	 */
	private void getEntitiesInBounds(Region region) {
		
	}

	/**
	 * Based on this regions position, checks which regions overlap it for
	 * entity loading.
	 */
	private void getOverlappingRegions() {
		Vector2f topRight = new Vector2f(botRight.x, topLeft.y);
		Vector2f botLeft = new Vector2f(topLeft.x, botRight.y);
		for (StaticRegion region : world.regions) {
			Vector2f rtl = region.topLeft, rbr = region.botRight;
			Vector2f rtr = new Vector2f(rbr.x, rtl.y);
			Vector2f rbl = new Vector2f(rtl.x, rbr.y);

			if ((topLeft.x < rbr.x && rbr.x < botRight.x)
					|| (rbr.y < topLeft.y && botRight.y < rbr.y)) {
				if (!overlappingRegions.contains(region)) {
					overlappingRegions.add(region);
				}
			}
		}
	}

	/**
	 * @return The number of polygons in the dynamic region.
	 */
	public int numPolys() {
		return numberPolygons;
	}

	/**
	 * Checks the list of overlapping regions to see if this region overlaps a
	 * static region <em>(within the bounds of <code>weight</code>)</em>
	 *
	 * @return <code>true</code> if this region overlaps a static region.
	 */
	private boolean overlapsRegion() {
		boolean tx, ty, bx, by;
		List<StaticRegion> regions = overlappingRegions.isEmpty() ? world.regions
				: overlappingRegions;
		for (StaticRegion region : regions) {
			Vector2f rtl = region.topLeft, rbr = region.botRight;
			tx = (topLeft.x - weight < rtl.x && topLeft.x + weight > rtl.x);
			ty = (topLeft.y - weight < rtl.y && topLeft.y + weight > rtl.y);
			bx = (botRight.x - weight < rbr.x && botRight.x + weight > rbr.x);
			by = (botRight.y - weight < rbr.y && botRight.y + weight > rbr.y);

			// If all true, return true (is overlapping)
			if (tx && ty && bx && by) {
				overlappingRegions.clear();
				overlappingRegions.add(region);
				return true;
			}
		}
		return false;
	}

	/**
	 * Called if this region overlaps a static region, and this region has extra
	 * entities registered to it. This method iterates through all the
	 * registered entities and removes the ones not in the overlapped region.
	 */
	private void removeExtraEntities() {
		
	}

	/**
	 * Runs all checks and updates for dynamic world loading
	 */
	public void update() {
	}
}
