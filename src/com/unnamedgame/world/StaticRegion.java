package com.unnamedgame.world;

import org.lwjgl.util.vector.Vector2f;

public class StaticRegion extends Region {

	StaticRegion(String name, Vector2f topLeft, Vector2f bottomRight) {
		super(name);
		this.topLeft = topLeft;
		this.botRight = bottomRight;
	}

	public String name() {
		return name;
	}
}
