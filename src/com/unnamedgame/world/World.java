package com.unnamedgame.world;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import org.newdawn.slick.geom.Rectangle;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.entity.Player;

/**
 * World class holds all the information for the game world.
 * 
 * @author Ryan Porterfield
 */
public class World implements Serializable {
	private static final long serialVersionUID = 1375087207081346317L;
	
	/**
	 * All the static regions that compose the world
	 */
	List<StaticRegion> regions;
	/**
	 * The region built around the player
	 */
	private DynamicRegion playerRegion;
	/**
	 * 
	 */
	private Logger logger;
	/**
	 * 
	 */
	private Player player;
	
	private String worldName;

	static public World constructWorldFromDatabase() {
		// TODO Read in World from database
		return null;
	}

	public World(String name) {
		this.worldName = name;
		this.logger = Logger.getLogger(UnnamedGame.LOGGER);
		regions = new ArrayList<>();
		setPlayer(new Player("Player 1"));
		logger.info("New world: " + name + " created!");
		constructTestWorld();
	}

	private void constructTestWorld() {
		Random rand = new Random(System.currentTimeMillis());
		for (int i = 0; i < 1000; i += 16) {
		}
	}
	
	public String getName() {
		return worldName;
	}

	public Player getPlayer() {
		return player;
	}

	public void registerEntity() {
		
	}

	private void setPlayer(Player player) {
		this.player = player;
	}

	public void unregisterEntity() {
		
	}

	public void update() {
		playerRegion.update();
	}
}
