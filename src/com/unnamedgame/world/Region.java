package com.unnamedgame.world;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.lwjgl.util.vector.Vector2f;


public abstract class Region {

	final String name;

	/**
	 *
	 */
	List<UUID> regionEntities;

	Vector2f topLeft;

	Vector2f botRight;

	Region(String name) {
		this.name = name;
		this.topLeft = new Vector2f(0F, 0F);
		this.botRight = new Vector2f(0F, 0F);
		regionEntities = new ArrayList<>();
	}
}
