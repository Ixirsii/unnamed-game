package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

import com.unnamedgame.entity.Player;

/**
 * Key event watches for the left movement key to be pressed, then updates the
 * players velocity.
 * 
 * @author Ryan Porterfield
 */
public class LeftEvent extends PlayerEventListener {
	/**
	 * Create a new Left event.
	 */
	public LeftEvent(Player player) {
		super(Input.KEY_A, player);
	}

	@Override
	public void onEvent(boolean keyState) {
		logger.finest("[Key] Left Event Fired!");
		player.moveLeft(keyState);
	}

}
