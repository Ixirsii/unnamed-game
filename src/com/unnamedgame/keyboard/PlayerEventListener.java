package com.unnamedgame.keyboard;

import com.unnamedgame.entity.Player;

/**
 * PlayerEventListener is a KeyEvenListener that effects the player when
 * pressed. For this, we need to hold a reference to the player being affected. 
 * 
 * @author Ryan Porterfield
 */
public abstract class PlayerEventListener extends KeyEventListener{
	/**
	 * 
	 */
	final Player player;
	
	/**
	 * 
	 */
	public PlayerEventListener(int key, Player player) {
		super(key);
		this.player = player;
	}

}
