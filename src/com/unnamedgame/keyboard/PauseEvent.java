package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

/**
 * Key event watches for the pause key to be pressed, changes game state and
 * renders the pause menu, provided the game is in a state that can be paused.
 * 
 * @author Ryan Porterfield
 */
public class PauseEvent extends KeyEventListener {
	/**
	 * Create a new Pause event.
	 */
	public PauseEvent() {
		super(Input.KEY_P);
	}

	@Override
	public void onEvent(boolean keyState) {
		logger.finest("[Key] Pause Event Fired!");
	}

}
