package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

import com.unnamedgame.entity.Player;

/**
 * Key event watches for the forward movement key to be pressed, then updates
 * the players velocity.
 * 
 * @author Ryan Porterfield
 */
public class ForwardEvent extends PlayerEventListener {
	/**
	 * Create a new Forward event.
	 */
	public ForwardEvent(Player player) {
		super(Input.KEY_W, player);
	}
	
	@Override
	public void onEvent(boolean keyState) {
		logger.finest("[Key] Forward Event Fired!");
		player.moveForward(keyState);
	}

}
