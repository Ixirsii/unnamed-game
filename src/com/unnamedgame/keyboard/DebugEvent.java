package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.state.GameState;

/**
 * Key event watches for the debug key to be pressed, then tries to render the
 * debug information on screen.
 * 
 * @author Ryan Porterfield
 */
public class DebugEvent extends KeyEventListener {
	
	/**
	 * Initialize a new Debug event drawing to the renderer.
	 * @param renderer What renderer is the debug information drawn to.
	 */
	public DebugEvent() {
		super(Input.KEY_F3);
	}

	@Override
	public void onEvent(boolean keyState) {
		logger.finest("[Key] Debug Event Fired!");
		if (!keyState)
			return;
		GameState state = (GameState) UnnamedGame.GAME.getCurrentState();
		state.getRender().toggleDebugMode();
	}

}
