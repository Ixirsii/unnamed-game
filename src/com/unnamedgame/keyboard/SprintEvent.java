package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

import com.unnamedgame.entity.Player;

/**
 * Key event watches for the sprint movement key to be pressed, then updates the
 * players velocity.
 * 
 * @author Ryan Porterfield
 */
public class SprintEvent extends PlayerEventListener {
	/**
	 * Create a new Sprint event.
	 */
	public SprintEvent(Player player) {
		super(Input.KEY_LSHIFT, player);
	}

	@Override
	public void onEvent(boolean keyState) {
		logger.finest("[Key] Sprint Event Fired!");
	}

}
