package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.state.CleanUpState;

/**
 * Key event watches for the quit key to be pressed, then changes the GameState,
 * cleans up resources, and exits.
 * 
 * @author Ryan Porterfield
 */
public class QuitEvent extends KeyEventListener {
	public QuitEvent() {
		super(Input.KEY_ESCAPE);
	}

	@Override
	public void onEvent(boolean keyState) {
		logger.finest("[Key] Quit Event Fired!");
		UnnamedGame.GAME.enterState(CleanUpState.ID);
	}

}
