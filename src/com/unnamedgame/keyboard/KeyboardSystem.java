package com.unnamedgame.keyboard;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;


/**
 * Process Keyboard input.
 * 
 * @author Ryan Porterfield
 */
public class KeyboardSystem implements KeyListener {
	/**
	 * List of all keyEventListeners which are "listening".
	 */
	private final Map<Integer, KeyEventListener> keyListeners;

	public KeyboardSystem() {
		keyListeners = new HashMap<>();
	}

	/**
	 * @see org.newdawn.slick.ControlledInputReciever#inputEnded()
	 */
	@Override
	public void inputEnded() {
	}

	/**
	 * @see org.newdawn.slick.ControlledInputReciever#inputStarted()
	 */
	@Override
	public void inputStarted() {
	}

	/**
	 * @see org.newdawn.slick.ControlledInputReciever#isAcceptingInput()
	 */
	@Override
	public boolean isAcceptingInput() {
		return true;
	}

	private void keyEvent(int key, boolean state) {
		KeyEventListener k = keyListeners.get(key);
		if (k != null)
			k.onEvent(state);
	}
	
	/**
	 * @see org.newdawn.slick.KeyListener#keyPressed(int, char)
	 */
	@Override
	public void keyPressed(int key, char c) {
		keyEvent(key, true);
	}

	/**
	 * @see org.newdawn.slick.KeyListener#keyReleased(int, char)
	 */
	@Override
	public void keyReleased(int key, char c) {
		keyEvent(key, false);
	}
	
	/**
	 * Add a new key event to watch for.
	 *   
	 * @param key KeyEventListener
	 */
	public void registerKeyEvent(KeyEventListener key) {
		keyListeners.put(key.getKeyCode(), key);
	}

	/**
	 * @see org.newdawn.slick.ControlledInputReciever#setInput(Input)
	 */
	@Override
	public void setInput(Input input) {
	}
	
	/**
	 * Remove a key event from being watched.
	 * 
	 * @param key KeyEventListener
	 */
	public void unregisterKeyEvent(KeyEventListener key) {
		keyListeners.remove(key);
	}
}
