package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

/**
 * Key event watches for the backpack key to be pressed, then opens the backpack
 * menu.
 * 
 * @author Ryan Porterfield
 */
public class BackpackEvent extends KeyEventListener {
	/**
	 * Create a new BackpackEvent
	 */
	public BackpackEvent() {
		super(Input.KEY_B);
	}

	@Override
	public void onEvent(boolean keyState) {
		// TODO Auto-generated method stub
		logger.finest("[Key] Backpack Event Fired!");
	}

}
