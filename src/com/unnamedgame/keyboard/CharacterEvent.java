package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

/**
 * Key event watches for the character key to be pressed, then opens the
 * character menu.
 * 
 * 
 * @author Ryan Porterfield
 */
public class CharacterEvent extends KeyEventListener {
	/**
	 * Create a new Character event.
	 */
	public CharacterEvent() {
		super(Input.KEY_C);
	}

	@Override
	public void onEvent(boolean keyState) {
		// TODO Auto-generated method stub
		logger.finest("[Key] Character Event Fired!");
	}

}
