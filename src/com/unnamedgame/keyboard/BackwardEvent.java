package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

import com.unnamedgame.entity.Player;

/**
 * Key event watches for the backward movement key to be pressed, then updates
 * the players velocity.
 * 
 * @author Ryan Porterfield
 */
public class BackwardEvent extends PlayerEventListener {
	/**
	 * Create a new backward (movement) event.
	 */
	public BackwardEvent(Player player) {
		super(Input.KEY_S, player);
	}
	
	@Override
	public void onEvent(boolean keyState) {
		logger.finest("[Key] Backward Event Fired!");
		player.moveBackward(keyState);
	}

}
