package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

import com.unnamedgame.entity.Player;

/**
 * Key event watches for the crouch key to be pressed, then updates the player
 * accordingly.
 * 
 * @author Ryan Porterfield
 */
public class CrouchEvent extends PlayerEventListener {
	/**
	 * Create a new Crouch event.
	 */
	public CrouchEvent(Player player) {
		super(Input.KEY_LCONTROL, player);
	}

	@Override
	public void onEvent(boolean keyState) {
		logger.finest("[Key] Crouch Event Fired!");
	}

}
