package com.unnamedgame.keyboard;

import org.newdawn.slick.Input;

import com.unnamedgame.entity.Player;

/**
 * Key event watches for the right movement key to be pressed, then updates the
 * players velocity.
 * 
 * @author Ryan Porterfield
 */
public class RightEvent extends PlayerEventListener {
	/**
	 * Create a new Right event.
	 */
	public RightEvent(Player player) {
		super(Input.KEY_D, player);
	}

	@Override
	public void onEvent(boolean keyState) {
		logger.finest("[Key] Right Event Fired!");
		player.moveRight(keyState);
	}

}
