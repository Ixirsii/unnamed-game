package com.unnamedgame.keyboard;

import java.util.logging.Logger;

import com.unnamedgame.UnnamedGame;

/**
 * Base KeyEvent class handles general KeyEvent behaviors such as setting,
 * getting, and keeping track of the actual key binding.
 * 
 * @author Ryan Porterfield
 */
public abstract class KeyEventListener {
	/**
	 * Log events and stuff.
	 */
	Logger logger = Logger.getLogger(UnnamedGame.LOGGER);
	/**
	 * The LWJGL key-binding integer value
	 */
	private int keyCode;

	/**
	 * Create a new KeyEvent bound to key.
	 * 
	 * @param key
	 */
	public KeyEventListener(int key) {
		this.keyCode = key;
	}

	public boolean checkEvent(int keyCode) {
		return keyCode == this.keyCode;
	}
	
	public int getKeyCode() {
		return keyCode;
	}

	public void setKey(int key) {
		this.keyCode = key;
	}
	
	public abstract void onEvent(boolean keyState);
}
