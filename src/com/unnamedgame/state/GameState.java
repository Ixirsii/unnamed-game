package com.unnamedgame.state;

import java.util.logging.Logger;

import org.newdawn.slick.state.BasicGameState;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.keyboard.KeyboardSystem;
import com.unnamedgame.render.BaseRender;

/**
 * 
 * @author Ryan Porterfield
 */
public abstract class GameState extends BasicGameState {
	/**
	 * State ID so Slick 2D knows which state to change to.
	 */
	final public int id;
	final KeyboardSystem keyboard;
	/**
	 * Game logger for debugging.
	 */
	Logger logger;
	/**
	 * Render class which will do all the rendering
	 */
	BaseRender render;

	
	/**
	 * 
	 * @param id
	 * @param keyboard
	 * @param render
	 */
	GameState(int id, KeyboardSystem keyboard, BaseRender render) {
		this.logger = Logger.getLogger(UnnamedGame.LOGGER);
		this.id = id;
		this.keyboard = keyboard;
		this.render = render;
	}

	@Override
	public final int getID() {
		return id;
	}
	
	public BaseRender getRender() {
		return render;
	}
}
