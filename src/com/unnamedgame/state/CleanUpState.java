package com.unnamedgame.state;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.keyboard.KeyboardSystem;
import com.unnamedgame.render.SplashRender;

public class CleanUpState extends GameState {
	static final public int ID = 3;

	public CleanUpState(KeyboardSystem keyboard) {
		super(ID, keyboard, new SplashRender());
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		logger.info("Clean up state created.");
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		logger.info("[CleanUp] Writing Settings");
		UnnamedGame.GAME.getSettings().save();
		container.exit();
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		render.render(container, game, g);
	}
}
