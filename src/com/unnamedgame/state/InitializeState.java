package com.unnamedgame.state;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.keyboard.DebugEvent;
import com.unnamedgame.keyboard.KeyboardSystem;
import com.unnamedgame.keyboard.QuitEvent;
import com.unnamedgame.render.SplashRender;
import com.unnamedgame.settings.Settings;

public class InitializeState extends GameState {
	static final public int ID = 0;
	
	public InitializeState() {
		super(ID, new KeyboardSystem(), new SplashRender());
	}

	private void changeState(GameContainer container, StateBasedGame game)
			throws SlickException {
		MainMenuState m = new MainMenuState(keyboard);
		m.init(container, game);
		game.addState(m);
		game.enterState(MainMenuState.ID);
	}
	
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		logger.info("Initialize state created.");
		container.getInput().addKeyListener(keyboard);
		container.setShowFPS(false);
	}
	
	private void registerKeyEvents() {
		keyboard.registerKeyEvent(new QuitEvent());
		keyboard.registerKeyEvent(new DebugEvent());
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		render.render(container, game, g);
	}
	
	private void settings(GameContainer container) {
		Settings s = UnnamedGame.GAME.getSettings();
		s.readSettings();
		container.setVSync(s.getVsync());
		container.setTargetFrameRate(s.getFPS());
	}
	
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		logger.info("Final initializations");
		logger.info(org.lwjgl.Sys.getVersion());
		registerKeyEvents();
		settings(container);
		container.getGraphics().setAntiAlias(true);
		changeState(container, game);
	}

}
