package com.unnamedgame.state;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.unnamedgame.keyboard.KeyboardSystem;
import com.unnamedgame.menu.ContinueButton;
import com.unnamedgame.menu.LoadButton;
import com.unnamedgame.menu.NewGameButton;
import com.unnamedgame.menu.SettingsButton;
import com.unnamedgame.world.World;

public class MainMenuState extends MenuState {
	static final public int ID = 1;

	/**
	 *
	 */
	public MainMenuState(KeyboardSystem keyboard) {
		super(ID, keyboard);
	}

	private void addStates(GameContainer container, StateBasedGame game)
			throws SlickException {
		CleanUpState c = new CleanUpState(keyboard);
		NewGameState n = new NewGameState(keyboard);
		RunningState r = new RunningState(keyboard, new World(
				"A Whole New World"));

		c.init(container, game);
		n.init(container, game);
		r.init(container, game);
		
		game.addState(c);
		game.addState(n);
		game.addState(r);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		addStates(container, game);
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		logger.info("Main Menu created");
		menu.addButton(new ContinueButton(10, 160));
		menu.addButton(new NewGameButton(10, 220));
		menu.addButton(new LoadButton(10, 280));
		menu.addButton(new SettingsButton(10, 340));
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		render.render(container, game, g);
	}

	/**
	 * @see com.unnamedgame.state.GameState#update(StateBasedGame)
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		//
	}
}
