package com.unnamedgame.state;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.unnamedgame.entity.Camera;
import com.unnamedgame.entity.Player;
import com.unnamedgame.keyboard.BackwardEvent;
import com.unnamedgame.keyboard.ForwardEvent;
import com.unnamedgame.keyboard.KeyboardSystem;
import com.unnamedgame.keyboard.LeftEvent;
import com.unnamedgame.keyboard.RightEvent;
import com.unnamedgame.render.GameRenderer;
import com.unnamedgame.world.World;

public class RunningState extends GameState {
	static final public int ID = 4;
	
	/**
	 * A StateRunning can have only one world instance. This is enforced by
	 * making the user go back to the MainMenuSate to choose a different world,
	 * which would create a new StateRunning instance.
	 * 
	 * The world is a reference to the World object that holds all the data for
	 * the current game.
	 */
	final private World world;
	
	private Camera camera;

	/**
	 * Initialize a new Running GameState.
	 * 
	 * @param game
	 * @param world
	 * @param keyboard
	 */
	public RunningState(KeyboardSystem keyboard, World world) {
		super(ID, keyboard, new GameRenderer(world));
		this.world = world;
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		container.getGraphics().setBackground(Color.white);
		Player p = world.getPlayer();
		this.camera = new Camera(p, container);
		((GameRenderer) render).setCamera(camera);
		
		keyboard.registerKeyEvent(new BackwardEvent(p));
		keyboard.registerKeyEvent(new ForwardEvent(p));
		keyboard.registerKeyEvent(new LeftEvent(p));
		keyboard.registerKeyEvent(new RightEvent(p));
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		logger.info("Running");
	}

	public World getWorld() {
		return world;
	}

	public void mainLoop() {
		world.update();
	}

	public void onStart() {
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		render.render(container, game, g);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		camera.update(container, game, delta);
		world.getPlayer().update(delta);
	}
}
