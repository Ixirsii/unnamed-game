package com.unnamedgame.state;

import com.unnamedgame.keyboard.KeyboardSystem;
import com.unnamedgame.menu.Menu;
import com.unnamedgame.menu.MenuButton;
import com.unnamedgame.render.MenuRenderer;

/**
 * 
 * @author Ryan Porterfield
 */
public abstract class MenuState extends GameState {
	final Menu menu;
	
	public MenuState(int id, KeyboardSystem keyboard) {
		super(id, keyboard, new MenuRenderer());
		menu = ((MenuRenderer) render).getMenu();
	}

	
	/**
	 * @see org.newdawn.slick.InputListener#mousePressed(int, int, int)
	 */
	@Override
	public void mousePressed(int button, int x, int y) {
		MenuButton b;
		if ((b = menu.getButton(x, y)) != null)
				b.onPress(button);
	}

	/**
	 * @see org.newdawn.slick.InputListener#mouseReleased(int, int, int)
	 */
	@Override
	public void mouseReleased(int button, int x, int y) {
		MenuButton b;
		if ((b = menu.getButton(x, y)) != null)
			b.onRelease(button);
	}
}
