package com.unnamedgame.item;

/**
 * 
 * @author Ryan Porterfield
 */
public abstract class Item {
	/** 
	 * The entity level required to use this item. This applies to both NPC's
	 * and player characters.
	 */
	final short requiredLevel;
	/**
	 * The base <em>strength</em> of the item. For every item and type of item
	 * this number will be influenced by player level, efficiency, as well
	 * as the enemy's stats.
	 * <ul>
	 * <li>For armor, this is how much damage is negated.
	 * <li>For weapons, this is how much damage is dealt
	 * <li>For potions this is how much effect the potion has.
	 * <li>etc.
	 * </ul>
	 */
	final int strength;
	
	public Item(short level, int strength) {
		this.requiredLevel = level;
		this.strength = strength;
	}
	
	/**
	 * Checks whether or not the player/NPC can use this item.
	 * 
	 * @param level 
	 * @return
	 */
	public boolean canUse(int level) {
		return level > requiredLevel;
	}
	
	public int getStrength() {
		return strength;
	}
}
