package com.unnamedgame.item;

/**
 * 
 * @author Ryan Porterfield
 */
public abstract class Armor extends Item {
	/**
	 * How much damage does a single point of armor strength buff. I.E. at 0.25
	 * every 4 points of armor strength will negate 1 point of attack damage.
	 * 
	 * @see com.unnamedgame.item.Item#strength
	 */
	static final private double DAMAGE_BUFF = 0.25; // TODO Balance this

	public Armor(short level, int strength) {
		super(level, strength);
	}

	/**
	 * Calculates how much damage the player will take after subtracting armor
	 * protection.
	 * 
	 * @param baseDamage
	 *            How much damage the enemy is trying to do.
	 * @return Modified damage.
	 */
	public int modifyDamage(int baseDamage) {
		return baseDamage - (int) (strength * DAMAGE_BUFF);
	}
}
