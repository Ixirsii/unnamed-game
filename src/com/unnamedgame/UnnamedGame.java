package com.unnamedgame;

/**
 * UnnamedGame application
 *
 * @author Ryan Porterfield
 * @author Justin Massey
 * @version Dev 2013-05-05
 */

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.unnamedgame.settings.Settings;
import com.unnamedgame.state.InitializeState;
import com.unnamedgame.util.HTMLFormatter;

public class UnnamedGame extends StateBasedGame {
	/**
	 * The String name we use to get a Logger. 
	 */
	static final public String LOGGER = "Unnamed Logger";
	/**
	 * Keep a reference to a game instance so we can easily grab it from
	 * external classes instead of passing around a reference.
	 */
	static final public UnnamedGame GAME = new UnnamedGame("UnnamedGame");

	/**
	 * System logger for logging debugging information
	 */
	private Logger logger = null;
	/**
	 * All game settings except key bindings reside here.
	 */
	private Settings settings = null;

	/*
	 * Try to load the DevUtils if they are present.
	 * Initialize and run the UnnamedGame instance.
	 */
	static public void main(String... args) {
		AppGameContainer container;
		try {
			container = new AppGameContainer(GAME);
			container.setDisplayMode(800, 450, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public UnnamedGame(String title) {
		super(title);
		logger = Logger.getLogger(LOGGER);
		initLogger();
		settings = new Settings();
	}

	/**
	 * Get an HTMLFormatter and set the file handler of the logger, so we can
	 * keep track of debug information.
	 */
	private void initLogger() {
		Formatter format = new HTMLFormatter();
		FileHandler handler = null;
		try {
			handler = new FileHandler(Settings.getUnnamedFolder().getPath()
					+ "/log.html", true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		handler.setFormatter(format);
		handler.setLevel(Level.ALL);
		logger.addHandler(handler);
		logger.setLevel(Level.ALL);
		logger.info("[Main] Starting Unnamed Game");
	}
	
	/**
	 * @return A reference to the game settings for parts of the program that
	 *         are setting-dependent.
	 */
	public Settings getSettings() {
		return settings;
	}

	/**
	 * Gets the time in milliseconds
	 * 
	 * @return time in milliseconds
	 */
	static public long getTime() {
		return System.nanoTime() / 1000000;
	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		addState(new InitializeState());
	}
}