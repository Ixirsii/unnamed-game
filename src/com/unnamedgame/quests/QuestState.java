package com.unnamedgame.quests;

/**
 * Quest Stauts
 * 
 * @author Ryan Porterfield
 *
 */
enum QuestState {
	ACTIVE(true, false, "Active"), COMPLETE(false, true, "Complete"), FAILED(false, true, "Failed"), 
	INACTIVE(false, false, "Inactive"), LOCKED(false, false, "Locked");
	
	private final boolean active;
	private final boolean complete;
	private final String name;
	
	QuestState(boolean active, boolean complete, String name) {
		this.active = active;
		this.complete = complete;
		this.name = name;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public boolean isCompleted() {
		return complete;
	}
	
	public String state() {
		return name;
	}
}
