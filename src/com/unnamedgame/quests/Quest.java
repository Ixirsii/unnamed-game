package com.unnamedgame.quests;

import com.unnamedgame.entity.Player;


/**
 * Basic Quest Type
 *
 * @author Ryan Porterfield
 *
 */
public abstract class Quest {
	/**
	 * A Null Quest, used for Quests without preliminary requirements, or resulting quests
	 */
	public static final Quest null_quest;
	/*
	 * An Array of Quests that have to be completed before this Quest is unlocked
	 */
	private final Quest[] preliminaryQuests;
	/*
	 * An Array of Quests that can be unlocked after this Quest is completed
	 */
	private final Quest[] unlockableQuests;
	/*
	 * The Quest Name
	 */
	private final String name;
	/*
	 * Enum - Quest Status
	 */
	private QuestState quest_state;

	public Quest(String name, Quest[] preliminaryQuests, Quest[] unlockableQuests) {
		this.name = name;
		this.quest_state = QuestState.LOCKED;
		this.preliminaryQuests = preliminaryQuests;
		this.unlockableQuests = unlockableQuests;
	}

	public boolean conditionsMet() {
		for(Quest condition : preliminaryQuests) {
			if(!condition.questState().equals(QuestState.COMPLETE.state()))
				return false;
		}
		return true;
	}

	public QuestState get_questState() {
		return this.quest_state;
	}

	public void addToActive(Player player) {

	}

	public void onComplete() {
		for(Quest resulting : unlockableQuests) {
			if(resulting.conditionsMet()) {
				//Add to Quest List
			}
		}
	}

	public String name() {
		return this.name;
	}

	public String questState() {
		return quest_state.state();
	}

	public void setQuestState(QuestState questState) {
		this.quest_state = questState;
	}
	public Quest[] unlockableQuests() {
		return this.unlockableQuests.clone();
	}

	static {
		null_quest = new NullQuest();
	}
}

class NullQuest extends Quest {
	protected NullQuest() {
		super("NULL", null, null);
	}
}
