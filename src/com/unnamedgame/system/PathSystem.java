package com.unnamedgame.system;

/**
 * 
 * @author Ryan Porterfield
 *
 */
public abstract class PathSystem {
	// Value Modifiers
	private float attack_modifier;
	private float defense_modifier;
	private float health_modifier;
	private float intelligence_modifier;
	private float resource_modifier;
	private float style_modifier;
	private float speed_modifier;
	
	
	public float getAttack_modifier() {
		return attack_modifier;
	}
	
	public float getDefense_modifier() {
		return defense_modifier;
	}
	
	public float getHealth_modifier() {
		return health_modifier;
	}
	
	public float getIntelligence_modifier() {
		return intelligence_modifier;
	}
	
	public float getResource_modifier() {
		return resource_modifier;
	}
	
	public float getStyle_modifier() {
		return style_modifier;
	}
	
	public float getSpeed_modifier() {
		return speed_modifier;
	}
	
	public void onLevelUp() {
		
	}
}
