package com.unnamedgame.system;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.world.World;

public class UpdateSystem implements Runnable {
	/**
	 * The world that this instance is running updates for
	 */
	final private World world;

	/**
	 * Is the update system running?
	 */
	private boolean running = true;
	/**
	 *  The time in milliseconds that the last major world update happened
	 */
	private long lastWorldUpdate;
	/**
	 * 
	 */
	private long sleepTime;

	/**
	 * @param player
	 *            The player the system is updating around
	 */
//	public UpdateSystem(Player player) {
//		this.player = player;
//	}
	public UpdateSystem(World world) {
		this.world = world;
		// Sleep time = milliseconds / cycles per second
		this.sleepTime = 1000 / 60;
	}

	public void majorUpdate() {
		lastWorldUpdate = UnnamedGame.getTime();
		// TODO World update
		// TODO update sleep time based on performance level (Settings)
	}

	public boolean needsWorldUpdate() {
		long currentTime = UnnamedGame.getTime();
		long delta = currentTime - lastWorldUpdate;
		return (delta / 1000) >= 10;
	}

	@Override
	public void run() {
		while( running ) {
			if( needsWorldUpdate() ) {
				majorUpdate();
			}
			world.update();
			try {
				Thread.sleep(sleepTime);
			} catch( InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void stop() {
		running = false;
	}
}
