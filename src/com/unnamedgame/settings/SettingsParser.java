package com.unnamedgame.settings;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * 
 * @author Ryan Porterfield
 */
abstract class SettingsParser implements ISettingsParser {
	
	final int settings[];
	final File settingsFile;
	final Logger logger;
	
	final private int version;
	
	SettingsParser(int version, int numSettings, Logger logger, File settingsFile) {
		this.version = version;
		settings = new int[numSettings];
		this.logger = logger;
		this.settingsFile = settingsFile;
	}

	/**
	 * Compares the integer parameter to {@link #version}. Since
	 * {@link #version} should only be changed when a change is made
	 * to settings so that existing settings files cannot be read in
	 * correctly.
	 * @param version
	 * @throws EOFException
	 */
	boolean checkSettingsVersion(int version) throws IOException {
		if (version < this.version) {
			logger.info("[Settings] Found an old settings version");
			return false;
		} else if (version > this.version) {
			throw new IOException("Wrong settings version");
		}
		return true;
	}

	/**
	 * Creates a new BufferedInputStream so the settingsFile can be read in.
	 * @return BufferedInputStream
	 * @see BufferedInputStream
	 * @see Settings#settingsFile
	 */
	private DataInputStream getInputStream(File settingsFile) {
		DataInputStream in = null;
		try {
			in = new DataInputStream(new FileInputStream(settingsFile));
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Could not find the settings file");
		}
		return in;
	}
	
	DataOutputStream getOutputStream(File settingsFile) {
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new FileOutputStream(settingsFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return out;
	}

	/**
	 * Reads the settings in from the settings file.
	 * 
	 * @param in
	 *            Pass the BufferedInputStream into the method so we don't have
	 *            to get it first.
	 * @throws IOException
	 *             if the settingsFile is too short, or if there is a problem
	 *             calling {@link BufferedInputStream#read()}
	 */
	private void getSettings(DataInputStream in) throws IOException {
		for (int i = 0; i < settings.length; i++) {
			int temp = in.readInt();
			if (temp == -1)
				throw new IOException("Settings file too short");
			settings[i] = temp;
		}
	}
	
	int getVersion() {
		return version;
	}

	/**
	 * Handle input exceptions from {@link #getSettings(BufferedInputStream)}
	 * 
	 * @see ISettingsParser#readSettings()
	 */
	@Override
	public void readSettings() {
		logger.info("[Settings] Reading in settings");
		try {
			DataInputStream in = getInputStream(settingsFile);
			if (!checkSettingsVersion(in.readInt()))
				oldSettings();
			else 
				getSettings(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			useDefaultSettings();
		}
	}

	/**
	 * Parse an older settings file.
	 */
	abstract void oldSettings();

	/**
	 * Set the key bindings from database in-case the user modified them.
	 */
	abstract void setKeyBindings();
	/**
	 * Sets the default settings values in {@link SettingsParser#settings} so
	 * we get real values (not uninitialized junk) later when we try to set
	 * settings.
	 */
	abstract void useDefaultSettings();
}
