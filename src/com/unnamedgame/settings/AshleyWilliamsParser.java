package com.unnamedgame.settings;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Settings parser for version 0x0 settings. Codename <b>Ashley Williams</b>
 * 
 * @author Ryan Porterfield
 */
class AshleyWilliamsParser extends SettingsParser {
	/**
	 * Create a new Ashley Williams version SettingsParser.
	 * 
	 * @param logger
	 *            Log events to console.
	 * @param settingsFile
	 *            Where are we looking for settings?
	 */
	AshleyWilliamsParser(Logger logger, File settingsFile) {
		super(0x1, 17, logger, settingsFile);
	}

	/**
	 * Get the frames per second as read from database.
	 * 
	 * @return fps
	 */
	public int getFPS() {
		return settings[0];
	}

	/**
	 * Get old window height as read from database.
	 * 
	 * @return height
	 */
	public int getHeight() {
		return settings[3];
	}

	/**
	 * Get mouse sensitivity as read from database.
	 * 
	 * @return sensitivity
	 */
	public int getSensitivity() {
		return settings[1];
	}

	/**
	 * Get old window width as read from database.
	 * 
	 * @return width
	 */
	public int getWidth() {
		return settings[2];
	}

	/**
	 * Get vsync status as read from database.
	 * 
	 * @return vsync
	 */
	public boolean getVsync() {
		return settings[4] != 0;
	}

	@Override
	public void oldSettings() {
		useDefaultSettings();
	}

	@Override
	public void saveSettings() {
		logger.info("[Settings] Writing current settings");
		try {
			DataOutputStream output = getOutputStream(settingsFile); 
			// Write version first!
			output.writeInt(getVersion());
			// Write settings
			for (int i = 0; i < settings.length; i++) {
				output.writeInt(settings[i]);
			}
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set the known key-bindings for this settings version.
	 */
	@Override
	public void setKeyBindings() {
		logger.info("[Settings] Setting key bindings");
		
	}

	/**
	 * Grabs the default keys from the value hard-coded in the class.
	 * Technically we will end up resetting all these values to what they
	 * already are in {@link #setKeyBindings()}, but it's easier than checking
	 * per-key if the value has been altered by the user.
	 */
	private void useDefaultKeys() {
		
	}

	@Override
	void useDefaultSettings() {
		settings[0] = Settings.AVERAGE; // Performance Level Default
		settings[1] = 1; // Mouse Sensitivity is Normal (1)
		settings[2] = 800; // Width - Target 16/9 ratio (16 / 2 = 8)
		settings[3] = 450; // Height - Target 16/9 ratio (9 / 2 = 4.5)
		settings[4] = 1; // Vsync (Boolean) 1 == True
		useDefaultKeys();
	}

}
