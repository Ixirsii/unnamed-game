package com.unnamedgame.settings;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import com.unnamedgame.UnnamedGame;

/**
 * Settings class handles everything 'settings' related. That is anything the
 * user/player can customize to suit their preferences. The Settings class also
 * handles reading and writing those preferences to a database so settings can
 * be saved between uses.
 * 
 * @author Ryan Porterfield
 */
public class Settings {
	/**
	 * Gaming rig FPS limit
	 */
	static final public int GAMING_RIG = 240;
	/**
	 * 
	 */
	static final public int HIGH_PERFORMANCE = 120;
	/**
	 * Default FPS limit
	 */
	static final public int AVERAGE = 60;
	/**
	 * Power saver FPS limit
	 */
	static final public int POWER_SAVER = 30;
	/**
	 * Low power machine FPS limit
	 */
	static final public int LIMITED = 20;
	
	/**
	 * File that holds all the external information for the game
	 */
	static final private File UNG_FOLDER = calculateFolderPath();
	/**
	 * File that settings are imported and exported to
	 */
	final private File settingsFile;

	/**
	 * Can debug mode be enabled or not?<br>
	 * This is set to <b>True</b> in the developer version, and <b>False</b>
	 * in the release version.
	 */
	private boolean debugMode = true;
	
	private boolean vsync;
	
	private int fps;
	
	private int height;
	
	private int sensitivity;
	
	private int width;
	/**
	 * For logging debug info and events to console
	 */
	private Logger logger;
	private AshleyWilliamsParser parser;

	/**
	 * Create a new settings instance
	 * 
	 * @param game Currently only used to get a logger instance.
	 */
	public Settings() {
		this(UNG_FOLDER);
	}
	
	public Settings(File ungFolder) {
		logger = Logger.getLogger(UnnamedGame.LOGGER);
		this.settingsFile = getSettingsFile(ungFolder);
		this.parser = new AshleyWilliamsParser(logger, settingsFile);
	}
	/**
	 * Get's a folder location equivalent to ~/Games/UnnamedGame under normal
	 * circumstances, or if user.home is not able to be returned, creates
	 * Games/UnnamedGame folder in the current directory.
	 * 
	 * @return A {@link java.io.File} pointing to the UnnamedGame folder.
	 * @see java.io.File
	 */
	static private File calculateFolderPath() {
		String app = System.getenv("APPDATA");
		app = (app != null) ? app : System.getProperty("user.home", ".");
		app += "/.unnamedgame";
		System.out.printf("app: %s\n", app);
		File file = new File(app);
		if (!file.exists() && !file.mkdirs()) {
			String e = "Could not access/create neccessary files & folders.";
			throw new RuntimeException(e);
		}
		System.out.printf("[Settings] UNG Folder Path: %s\n", file.getPath());
		return file;
	}

	/**
	 * Can debug mode be enabled or not?
	 * @return {@link Settings#debugMode}
	 */
	public boolean canShowDebug() {
		return debugMode;
	}

	/**
	 * How many Frames Per Second should we try and draw based on the
	 * performance level.
	 * @return Frames Per Second
	 * @see Settings#settings
	 * @see	Settings#FPS
	 */
	public int getFPS() {
		return fps;
	}
	
	/**
	 * How tall should the window frame be?
	 * @return Window height
	 * @see Settings#settings
	 * @see Settings#HEIGHT
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * What should the mouse sensitivity be?
	 * @return Mouse sensitivity
	 * @see Settings#settings
	 * @see Settings#SENSITIVITY
	 */
	public int getSensitivity() {
		return sensitivity;
	}

	private File getSettingsFile(File ungFolder) {
		ungFolder = (ungFolder != null) ? ungFolder : UNG_FOLDER;
		File f = new File(ungFolder, "settings.dat");
		try {
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Cannot create settings database");
		}
		return f;
	}
	/**
	 * How wide should the window frame be?
	 * @return Window height
	 * @see Settings#settings
	 * @see Settings#WIDTH
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Should VSync be enabled?
	 * @return VSync
	 * @see Settings#settings
	 * @see Settings#VSYNC
	 */
	public boolean getVsync() {
		return vsync;
	}

	/**
	 * Get a Java File Object that points to the UnnamedGame folder location.
	 * @return {@link #UNG_FOLDER}
	 */
	static public File getUnnamedFolder() {
		return UNG_FOLDER;
	}

	/**
	 * Use the SettingsParser to get settings from database
	 */
	public void readSettings() {
		parser.readSettings();
		// Log what we are doing
		logger.info("[Settings] Getting settings from parser");
		// Our FPS
		fps = parser.getFPS();
		logger.config("[Settings] FPS: " + fps);
		// Mouse sensitivity
		sensitivity = parser.getSensitivity();
		logger.config("[Settings] Mouse sensitivity: " + sensitivity);
		// Window width
		width = parser.getWidth();
		logger.config("[Settings] Window width: " + width);
		// Window height
		height = parser.getHeight();
		logger.config("[Settings] Window height: " + height);
		// VSync
		vsync = parser.getVsync();
		logger.config("[Settings] Vsync: " + vsync);
		// Custom key-bindings
		parser.setKeyBindings();
	}
	
	public void save() {
		parser.saveSettings();
	}
	
	public void setDebug() {
		debugMode = !debugMode;
	}
}