package com.unnamedgame.settings;


/**
 * 
 * @author Ryan Porterfield
 */
interface ISettingsParser {
	/**
	 * Read in settings from the database.
	 */
	public void readSettings();
	
	/**
	 * Write/Save settings to the database.
	 */
	public void saveSettings();
}
