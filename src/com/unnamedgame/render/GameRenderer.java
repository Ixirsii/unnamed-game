package com.unnamedgame.render;

import java.util.logging.Logger;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.entity.Camera;
import com.unnamedgame.entity.Player;
import com.unnamedgame.world.World;

/**
 * This class will probably be changed to an interface, with two implementing
 * classes, one for each of the draw methods that currently exist in this class.
 * At the very least, two new classes will be made which inherit from this,
 * which will be singletons, and will have one draw method each. <br />
 * <br />
 * 
 * @author Ryan Porterfield
 */
public class GameRenderer extends BaseRender {

	final private Logger logger;
	/**
	 * The inability to change world is enforced the same way as the inability
	 * to change StateRunning.
	 * 
	 * The world that the RenderSystem instance is drawing.
	 */
	final private World world;

	private Camera camera;
	private Player player;

	/**
	 * 
	 */
	public GameRenderer(World world) {
		this.logger = Logger.getLogger(UnnamedGame.LOGGER);
		this.world = world;
		this.player = world.getPlayer();
	}

	String createDebugString(GameContainer container) {
		StringBuilder debug = new StringBuilder(
				super.createDebugString(container));
		debug.append("\n   Player    Camera\nx: ");
		debug.append(format.format(player.getX())).append("       ");
		if (player.getX() < 100) 
			debug.append(' ');
		debug.append(format.format(camera.getX())).append("\ny: ");
		debug.append(format.format(player.getY())).append("       ");
		if (player.getY() < 100) 
			debug.append(' ');
		debug.append(format.format(camera.getY())).append("\norientation: ");
		debug.append(format.format(player.getOrientation()));
		return debug.toString();
	}

	/**
	 * Disables 3D drawing and dynamic lighting, and draws the Heads Up Display
	 * (HUD) and checks to draw debug
	 */
	private void drawHUD(Graphics g) {

	}

	/**
	 * 
	 */
	private void drawWorld(Graphics g) {
		if (camera == null) {
			logger.severe("[Game Render] Camera is null");
			return;
		}
		player.draw(g, camera);
		g.setColor(Color.green);
		Rectangle r = camera.getBox();
		g.drawRect(r.getX() - camera.getX(), r.getY() - camera.getY(),
				r.getWidth(), r.getHeight());
	}

	/**
	 * Setter method for the camera
	 * 
	 * @param camera
	 */
	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	/**
	 * Draws the game world and entities on the screen
	 */
	@Override
	public void stateRender(GameContainer container, StateBasedGame game,
			Graphics g) throws SlickException {
		drawWorld(g);
		drawHUD(g);
	}
}
