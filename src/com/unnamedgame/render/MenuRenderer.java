package com.unnamedgame.render;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.unnamedgame.menu.Menu;

public class MenuRenderer extends BaseRender {
	final private Menu menu;
	
	public MenuRenderer() {
		menu = new Menu();
		debugColor = Color.white;
	}
	
	public Menu getMenu() {
		return menu;
	}

	@Override
	public void stateRender(GameContainer container, StateBasedGame game,
			Graphics g) throws SlickException {
		menu.render(g);
	}
}
