package com.unnamedgame.render;

import java.io.InputStream;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * 
 * @author Ryan Porterfield
 */
public class SplashRender extends BaseRender {
	/**
	 * 
	 */
	private Image splash;
	
	public SplashRender() {
		init();
	}
	
	private void init() {
		InputStream in = getClass().getResourceAsStream("/res/splash.png");
		try {
			splash = new Image(in, "splash", false);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stateRender(GameContainer container, StateBasedGame game,
			Graphics g) throws SlickException {
		g.drawImage(splash, 0, 0);
	}
}
