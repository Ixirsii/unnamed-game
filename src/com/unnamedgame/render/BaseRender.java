package com.unnamedgame.render;

import static java.awt.Font.PLAIN;

import java.awt.Font;
import java.text.DecimalFormat;
import java.util.logging.Logger;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.StateBasedGame;

import com.unnamedgame.UnnamedGame;

public abstract class BaseRender {
	static final private long MiB = 1024 * 1024;
	/**
	 * FPS formatter
	 */
	static public DecimalFormat format = new DecimalFormat("00");
	/**
	 * Full Version String of OpenGL on the host machine
	 */
	static private String openGLVersionString = GL11
			.glGetString(GL11.GL_VERSION);
	/**
	 * Draws strings to the display
	 */
	static public TrueTypeFont font = getFont();

	/**
	 * 
	 */
	final Logger logger;

	private long allocated;
	/**
	 * What color to draw the debug text in
	 */
	Color debugColor;
	/**
	 * How much memory is available to the JVM
	 */
	private long max;
	/**
	 * What percentage of the maximum memory has been allocated.
	 */
	private float percentageAllocated;
	/**
	 * What percentage of JVM memory is in use
	 */
	private float percentageUsed;
	/**
	 * How many processors does the machine have
	 */
	private int processors;
	/**
	 * Get CPU and RAM information
	 */
	private Runtime runtime;
	/**
	 * Display debug info
	 */
	private boolean showDebug;
	/**
	 * String formated to look like: <em>"Processors: #"</em> with a trailing
	 * newline character.
	 */
	private String str_processors;
	/**
	 * How much memory is the JVM using
	 */
	private long used;

	/**
	 * 
	 */
	abstract void stateRender(GameContainer container, StateBasedGame game,
			Graphics g) throws SlickException;

	/**
	 * 
	 * @param cam
	 */
	public BaseRender() {
		this.logger = Logger.getLogger(UnnamedGame.LOGGER);
		this.debugColor = Color.black;
		this.showDebug = true;
		this.runtime = Runtime.getRuntime();
		init();
	}

	private void init() {
		max = runtime.maxMemory() / MiB;
		processors = runtime.availableProcessors();
		str_processors = "Processors: " + processors + '\n';
	}

	/**
	 * Takes previously calculated memory information, stored in instance
	 * variables, and creates a string to display the information in a useful
	 * manner.
	 * 
	 * @return Memory usage information string.
	 */
	String buildMemString() {
		calculateMemUsage();
		StringBuilder mem = new StringBuilder("Used: ");
		mem.append(format.format(percentageUsed)).append("% (");
		mem.append(used).append("MiB) / ").append(max).append("MiB\n");
		mem.append("Allocated: ").append(format.format(percentageAllocated));
		mem.append("% (").append(allocated).append("MiB)\n");
		mem.append(str_processors);
		return mem.toString();
	}

	/**
	 * Gets information from the Runtime to caclulate how much memory is
	 * allocated and being used, and what percentage of the maximum memory
	 * each value is.
	 * 
	 * @see java.lang.Runtime
	 */
	private void calculateMemUsage() {
		allocated = runtime.totalMemory() / MiB;
		used = allocated - (runtime.freeMemory() / MiB);
		percentageAllocated = ((float) allocated / max) * 100F;
		percentageUsed = ((float) used / max) * 100F;
	}

	/**
	 * Takes the relevant debug information, and concatenates it for writing to
	 * the screen.
	 * 
	 * @return debug string
	 */
	String createDebugString(GameContainer container) {
		StringBuilder b = new StringBuilder("FPS: ");
		b.append(container.getFPS()).append("\n").append(openGLVersionString);
		return b.toString();
	}

	/**
	 * Calls createDebugString(GameContainer) to create the debug string, then
	 * uses the Graphics to draw the string to the screen.
	 * 
	 * @param container
	 *            The GameContainer that we will used for FPS information
	 * @param g
	 *            The Graphics that will be used to draw to the screen.
	 * 
	 * @see com.unnamedgame.render.BaseRender#createDebugString(GameContainer)
	 * @see org.newdawn.slick.GameContainer
	 * @see org.newdawn.slick.Graphics
	 */
	void drawDebug(GameContainer container, Graphics g) {
		String debugInfo = createDebugString(container);
		g.setColor(debugColor);
		g.drawString(debugInfo, 10, 10);
		drawMemoryUsage(g);
	}

	/**
	 * 
	 */
	void drawMemoryUsage(Graphics g) {
		String mem = buildMemString();
		g.drawString(mem, 525, 10);
	}

	static private TrueTypeFont getFont() {
		Font awtFont = new Font("Courier New", PLAIN, 12);
		TrueTypeFont f = new TrueTypeFont(awtFont, true);
		return f;
	}

	public final void render(GameContainer container, StateBasedGame game,
			Graphics g) throws SlickException {
		stateRender(container, game, g);
		if (showDebug) {
			drawDebug(container, g);
		}
	}

	/**
	 * Toggles the debug text
	 * 
	 * @param showDebug
	 */
	public void toggleDebugMode() {
		boolean canShow = UnnamedGame.GAME.getSettings().canShowDebug();
		if (canShow) {
			showDebug = !showDebug;
		} else if (!canShow && showDebug) {
			showDebug = false;
		}
	}
}
