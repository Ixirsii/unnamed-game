package com.unnamedgame.random;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import com.unnamedgame.entity.Player;
import com.unnamedgame.world.World;

/**
 * Store's random strings - for variety mostly.<br />
 * <br />
 * 
 * Wow this is an old class. I probably could remove it, but I'll leave it
 * in-case we use it for anything later.
 * 
 * @author Ryan Porterfield
 */
public abstract class Randoms {
	private final Map<String, String> vars;
	// "Hello %player and welcome the the wonderful world of %world!"
	Random random;

	public Randoms(Player player, World world) {
		this.random = new Random(System.nanoTime());
		vars = new HashMap<>();
		vars.put("%player", null);
		vars.put("%world", world.getName());
	}

	public String parseRandom(String random) {
		StringBuilder parsed = new StringBuilder();
		StringTokenizer rnd = new StringTokenizer(random);
		while (rnd.hasMoreTokens()) {
			String token = rnd.nextToken();
			token = (vars.get(token) == null) ? token : vars.get(token);
			parsed.append(token);
			parsed.append(' ');
		}
		parsed.replace(parsed.length() - 1, parsed.length(), ".");
		return parsed.toString();
	}
}
