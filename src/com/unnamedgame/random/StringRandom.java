package com.unnamedgame.random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.unnamedgame.entity.Player;
import com.unnamedgame.world.World;

/**
 * 
 * @author Ryan Porterfield
 */
public class StringRandom extends Randoms {
	private List<String> randoms;
	
	StringRandom(Player player, World world) {
		this(player, world, (String[]) null);
	}
	
	StringRandom(Player player, World world, String ...strings) {
		super(player, world);
		randoms = new ArrayList<>(Arrays.asList(strings));
	}
	
	public String getRandom() {
		return parseRandom(randoms.get(random.nextInt(randoms.size())));
	}
}
