package com.unnamedgame.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 
 * @author Ryan Porterfield
 */
public class ResourceLoader {
	/**
	 * 
	 */
	final private File resourceFile;

	public ResourceLoader(File resourceFile) {
		this.resourceFile = resourceFile;
	}

	/**
	 * Get a BufferedReader pointing to fileName in the UNG Resource Folderrrr.
	 * 
	 * @param fileName
	 *            Resources file name.
	 * @return new BufferedReader
	 */
	public BufferedReader getReader(String fileName) {
		File file = new File(resourceFile, fileName);
		if (!file.exists())
			return null;
		InputStream instream = null;
		BufferedReader reader = null;
		try {
			instream = new FileInputStream(file);
			reader = new BufferedReader(new InputStreamReader(instream));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return reader;
	}
}
