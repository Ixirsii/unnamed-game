package com.unnamedgame.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * 
 * @author Ryan Porterfield
 * @see Formatter
 * @see Logger
 */
public class HTMLFormatter extends Formatter {
	private int today;
	private Calendar calendar;
	/**
	 * Maps the {@link Level#intValue()} to an html color and the
	 * {@link Level#getName()}
	 */
	private Map<Integer, String> levelMap;
	private SimpleDateFormat date = new SimpleDateFormat("MMM dd, yyyy");
	private SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");

	public HTMLFormatter() {
		levelMap = new HashMap<>();
		calendar = Calendar.getInstance();
		today = calendar.get(Calendar.DAY_OF_MONTH);
		init();
	}

	/**
	 * Populate levelMap
	 */
	private void init() {
		String severe = "<font color=\"red\"><b>" + Level.SEVERE.getName();
		levelMap.put(Level.SEVERE.intValue(), severe);
		String warn = "<font color=\"orange\"><b>" + Level.WARNING.getName();
		levelMap.put(Level.WARNING.intValue(), warn);
		String info = "<font color=\"blue\"><b>" + Level.INFO.getName();
		levelMap.put(Level.INFO.intValue(), info);
		String config = "<font color=\"purple\"><b>" + Level.CONFIG.getName();
		levelMap.put(Level.CONFIG.intValue(), config);
		String fine = "<font color=\"darkgreen\"><b>" + Level.FINE.getName();
		levelMap.put(Level.FINE.intValue(), fine);
		String finer = "<font color=\"green\"><b>" + Level.FINER.getName();
		levelMap.put(Level.FINER.intValue(), finer);
		String f = "<font color=\"lightgreen\"><b>" + Level.FINEST.getName();
		levelMap.put(Level.FINEST.intValue(), f);
	}

	private boolean checkDate() {
		if (calendar.get(Calendar.DAY_OF_MONTH) != today)
			return true;
		return false;
	}

	/**
	 * Calculates the day and time, and returns it as a String in the
	 * International Date Format.
	 * 
	 * @param milliseconds
	 *            the time in milliseconds
	 * @return time in International Date Format
	 */
	public String getTime(long milliseconds) {
		return time.format(new Date(milliseconds));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 */
	@Override
	public String format(LogRecord rec) {
		StringBuilder log = new StringBuilder();
		if (checkDate())
			log.append(nextDay());
		log.append(getLevel(rec.getLevel().intValue()));
		String message = rec.getMessage();
		
		String title = " ";
		if (rec.getSourceClassName() != null)
			title = getTitle(rec.getSourceClassName());
		if (rec.getSourceMethodName() != null)
			title = modifyTitle(title, rec.getSourceMethodName());

		log.append("</td><td>");
		log.append(getTime(rec.getMillis()));
		log.append("</td><td>");
		log.append(title);
		log.append("</td><td>");
		log.append(message);
		log.append("</td></tr>\n");

		return log.toString();
	}

	@Override
	public String getHead(Handler h) {
		StringBuffer head = new StringBuffer("<html>\n<head>\n");
		head.append("\t<title>");
		head.append(h.getClass().getSimpleName());
		head.append(" Logger</title>\n");
		head.append("</head>\n<body>\n");
		head.append("\t<h1>").append(date.format(new Date())).append("</h1>\n");
		head.append("\t<pre>\n\t\t<table border>\n");
		head.append("\t\t\t<tr><th>LEVEL</th><th>TIME</th><th>TITLE</th>");
		head.append("<th>LOG MESSAGE</th></tr>\n");
		return head.toString();
	}

	/**
	 * Get a StringBuffer containing the {@link Logger#getLevel()} info.
	 * 
	 * @param logLevel
	 * @return StringBuffer
	 */
	private String getLevel(int logLevel) {
		return "\t\t\t<tr><td>" + levelMap.get(logLevel) + "</b></font>";
	}

	@Override
	public String getTail(Handler h) {
		return "\t\t</table>\n\t</pre>\n</BODY>\n</HTML>\n";
	}

	private String getTitle(String className) {
		String split[] = className.split("\\.");
		String text = className;
		if (split.length > 0)
			text = split[split.length - 1];
		return "[" + text + "]";
	}

	private String modifyTitle(String title, String method) {
		return title.substring(0, title.length() - 1) + " " + method + "]";
	}
	
	private String nextDay() {
		StringBuilder b = new StringBuilder("\t\t</table>\n\t</pre>\n\t");
		b.append("<h1>").append(date.format(new Date())).append("</h1>\n");
		b.append("\t<pre>\n\t\t<table border>\n");
		return b.toString();
	}
}
