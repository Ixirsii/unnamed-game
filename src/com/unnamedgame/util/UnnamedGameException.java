package com.unnamedgame.util;

public class UnnamedGameException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String message = "null";
	public UnnamedGameException() {
	}

	public UnnamedGameException(String arg0) {
		super(arg0);
		message = arg0;
	}

	public Throwable getCause() {
		return super.getCause();
	}
	public String getLocalizedMessage() {
		return super.getLocalizedMessage();
	}
	public String getMessage() {
		return super.getMessage();
	}
	public void printStackTrace() {
		super.printStackTrace();
	}
	public void printStackTrace(java.io.PrintStream s) {
		super.printStackTrace(s);
	}
	public void printStackTrace(java.io.PrintWriter s) {
		super.printStackTrace(s);
	}
	public String toString() {
		String text = "UnnamedException";
		if(!message.equals("null")) {
			text = text.concat(": " + message);
		}
		return text;
	}
}
