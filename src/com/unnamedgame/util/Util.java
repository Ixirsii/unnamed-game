package com.unnamedgame.util;

import java.nio.FloatBuffer;
import java.util.logging.Logger;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import com.unnamedgame.UnnamedGame;

public class Util {
	private final Logger logger;
	
	public Util() {
		this.logger = Logger.getLogger(UnnamedGame.LOGGER);
	}
	public int printGLError() {
		int errorCode = GL11.glGetError();
		if (errorCode == GL11.GL_NO_ERROR)
			return errorCode;
		logger.severe(GLU.gluErrorString(errorCode));
		return errorCode;
	}
	
	public FloatBuffer reserveData(int size) {
		return BufferUtils.createFloatBuffer(size);
	}
	
	public FloatBuffer toFloatBuffer(float values[]) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(values.length);
		for (float f : values) {
			buffer.put(f);
		}
		buffer.flip();
		return buffer;
	}
}
