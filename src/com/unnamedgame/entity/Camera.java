package com.unnamedgame.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

public class Camera {
	private float border;
	private Rectangle hitBox;
	private Player player;
	private int screenHeight;
	private int screenWidth;
	private float x;
	private float y;

	public Camera(Player player, GameContainer container) {
		this.player = player;
		init(container);
	}

	public Camera(float x, float y, GameContainer container) {
		this.x = x;
		this.y = y;
		init(container);
	}

	private void init(GameContainer container) {
		this.border = 100F;
		this.hitBox = new Rectangle(0, 0, 0, 0);
		onResize(container);
		translate(container);
	}

	public Rectangle getBox() {
		return this.hitBox;
	}
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public void onResize(GameContainer container) {
		this.screenHeight = container.getHeight();
		this.screenWidth = container.getWidth();
		this.hitBox.setX(this.x + border);
		this.hitBox.setY(this.y + border);
		this.hitBox.setHeight(screenHeight - (border * 2));
		this.hitBox.setWidth(screenWidth - (border * 2));
	}

	public void setBorder(float border, GameContainer container) {
		this.border = border;
		onResize(container);
	}

	private void translate(GameContainer container) {
		if (player == null
				|| hitBox.contains(player.getCenterX(), player.getCenterY()))
			return;
		translateX();
		translateY();
		onResize(container);
	}
	
	private void translateX() {
		// Take a point y, known to be in the box to check x specifically
		if (hitBox.contains(player.getCenterX(), hitBox.getY() + 1))
			return;
		x = Math.max(0, player.x - (screenWidth / 2));
	}
	
	private void translateY() {
		if (hitBox.contains(hitBox.getCenterX(), player.getCenterY()))
			return;
		y = Math.max(0, player.y - (screenHeight / 2));
	}

	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		translate(container);
	}
}
