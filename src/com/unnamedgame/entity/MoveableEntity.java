package com.unnamedgame.entity;

/**
 * 
 * @author Ryan Porterfield
 */
public abstract class MoveableEntity extends Entity {
	static final float WALKING_SPEED = 0.0625F;
	
	/**
	 * What direction the player is facing.
	 */
	double orientation;
	/** How fast is the entity moving **/
	float velocity;
	
	public MoveableEntity(String name, float x, float y, int health) {
		super(name, x, y, health);
		this.velocity = 0;
	}
	
	public double getOrientation() {
		return this.orientation;
	}
	
	public float getVelocity() {
		return this.velocity;
	}
	
	public void update(int delta) {
		super.update(delta);
		double rads = Math.toRadians(orientation);
		this.x = this.x + ((float) Math.sin(rads) * velocity) * delta;
		this.y = this.y - ((float) Math.cos(rads) * velocity) * delta;
	}
}
