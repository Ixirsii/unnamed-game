package com.unnamedgame.entity;

import java.io.IOException;
import java.io.InputStream;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * 
 * @author Ryan Porterfield
 */
public abstract class Human extends MoveableEntity {
	/** File location of the default player sprite **/
	static final String DEFAULT_SPRITE = "/res/player.png";

	/**
	 * Players collective experience
	 */
	int experience;
	/**
	 * Experience from current level to next level
	 */
	int expToNextLevel;
	/**
	 * The color of the player's skin.
	 */
	Color skinColor;
	/**
	 * Leg animation for top-down view
	 */
	Animation sprite;

	/**
	 * @param name
	 * @param x
	 * @param y
	 * @param health
	 */
	public Human(String name, float x, float y, int health) {
		super(name, x, y, health);
		// Regen rate set to regenerate full health in ten minutes
		this.regen = health / (10 * 60 * 1000);
		skinColor = Color.blue;
		sprite = getSprite();
	}

	/**
	 * @see com.unnamedgame.entity.Entity#draw(org.newdawn.slick.Graphics)
	 */
	@Override
	public void draw(Graphics g, Camera c) {
		g.pushTransform();
		g.rotate(getCenterX() - c.getX(), getCenterY() - c.getY(),
				(float) orientation);
		g.drawAnimation(sprite, x - c.getX(), y - c.getY(), skinColor);
		g.popTransform();
	}

	public float getCenterX() {
		return x + 24F;
	}

	public float getCenterY() {
		return y + 24F;
	}

	/**
	 * @return players total experience
	 */
	public int getExperience() {
		return experience;
	}

	/**
	 * @return how much experience the player needs to level up
	 */
	public int getExpToLevel() {
		return expToNextLevel;
	}

	private Animation getSprite() {
		InputStream in = getClass().getResourceAsStream(DEFAULT_SPRITE);
		Animation a = null;
		try {
			SpriteSheet ss = new SpriteSheet(name + " Sprite", in, 48, 48);
			a = new Animation(ss, 250);
			a.stop();
			a.setCurrentFrame(2);
			a.setPingPong(true);
			in.close();
		} catch (SlickException | IOException e) {
			e.printStackTrace();
		}
		return a;
	}

	public void move(boolean keyState, double orientation) {
		if (!keyState && orientation != this.orientation)
			return;
		this.orientation = orientation;
		if (keyState) {
			this.velocity = WALKING_SPEED;
			sprite.restart();
		} else {
			this.velocity = 0F;
			sprite.stop();
			sprite.setCurrentFrame(sprite.getFrameCount() / 2);
		}

	}

	public void moveBackward(boolean keyState) {
		move(keyState, 180D);
	}

	public void moveForward(boolean keyState) {
		move(keyState, 0D);
	}

	public void moveLeft(boolean keyState) {
		move(keyState, 270D);
	}

	public void moveRight(boolean keyState) {
		move(keyState, 90D);
	}

	/**
	 * @see com.unnamedgame.entity.MoveableEntity#update(int)
	 */
	@Override
	public void update(int delta) {
		super.update(delta);
		sprite.update(delta);
	}
}
