package com.unnamedgame.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Player is a meta entity. It does not implement or inherit anything that makes
 * it a meta entity, what makes it a meta entity is that it uses components and
 * is used by systems, but is to large to delegate to an EntityManager with all
 * it's components and methods it needs to have.
 * 
 * @author Ryan Porterfield
 */
public class Player extends Human implements Serializable {
	static final private long serialVersionUID = 5247811665144293773L;

	/**
	 * Resource is the currency used to invest in Tactics, Weapons, and Armor
	 */
	private int resource;
	/**
	 * Map of Saves by key Calendar
	 */
	private Map<Calendar, Player> saves = new HashMap<>();

	/**
	 * 
	 * @param name
	 */
	public Player(String name) {
		super(name, 376F, 201F, 20);
	}

	/**
	 * @return how much <em>resource</em> the player has
	 * 
	 * @see com.unnamedgame.entity.Player#resource
	 */
	public int getResource() {
		return resource;
	}

	/**
	 * This will do something soon
	 * 
	 * @param i
	 *            the index in the save list to save to
	 * @return <strong>true</strong> if successfully saved, else <strong>false
	 *         </strong>
	 */
	public boolean save(Calendar saveSlot) {
		try {
			saves.put(Calendar.getInstance(), this);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.severe("Error saving to: " + saveSlot.toString());
		}
		return false;
	}
	
	/**
	 * @see com.unnamedgame.entity.Human#update(int)
	 */
	@Override
	public void update(int delta) {
		super.update(delta);
	}
}
