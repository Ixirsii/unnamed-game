package com.unnamedgame.entity;

import java.util.logging.Logger;

import org.newdawn.slick.Graphics;

import com.unnamedgame.UnnamedGame;

public abstract class Entity {
	/**
	 * To keep track/log pertinent information.
	 */
	final Logger logger;
	
	/** Entity's current health; health remaining **/
	int health;
	/** Entity's maximum health; full health **/
	int maxHealth;
	/** The entity's name **/
	String name;
	/**
	 * How fast does the entity's health regenerate. 
	 * Value in health points per millisecond.
	 */
	double regen;
	/** Entity's left coordinate **/
	float x;
	/** Entity's top coordinate **/
	float y;

	/**
	 * Initialize a new entity.
	 * 
	 * @param name
	 *            What the entity will be called.
	 * @param x
	 *            The entity's x coordinate.
	 * @param y
	 *            The entity's y coordinate.
	 * @param health
	 *            The entity's health.
	 */
	public Entity(String name, float x, float y, int maxHealth) {
		logger = Logger.getLogger(UnnamedGame.LOGGER);
		this.name = name;
		this.x = x;
		this.y = y;
		this.maxHealth = maxHealth;
		this.health = maxHealth;
		// TODO Balance this - Regen rate ~ 1000hp/30m (divided by 1000 for ms)
		this.regen = 0.5 / 1000;
	}

	/**
	 * What happens when the entity dies?
	 */
	public void die() {
		// TODO die
	}

	/**
	 * Draw the entity's image to the screen.
	 * 
	 * @param g
	 */
	public abstract void draw(Graphics g, Camera c);

	/**
	 * @return The entity's name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return The entity's x coordinate.
	 */
	public float getX() {
		return this.x;
	}

	/**
	 * @return The entity's y coordinate.
	 */
	public float getY() {
		return this.y;
	}

	/**
	 * Equivalent to getting the x coordinate, adding distance moved, and
	 * setting the x coordinate. This method adds x to the entity's x for easier
	 * movement code.
	 * 
	 * @param x
	 *            How far the entity will move in the x direction.
	 * @return this
	 */
	public Entity moveX(float x) {
		this.x = this.x + x;
		return this;
	}

	/**
	 * Equivalent to getting the y coordinate, adding distance moved, and
	 * setting the x coordinate. This method adds y to the entity's y for easier
	 * movement code.
	 * 
	 * @param y
	 *            How far the entity will move in the y direction.
	 * @return this
	 */
	public Entity moveY(float y) {
		this.y = this.y + y;
		return this;
	}

	/**
	 * Changes the entity's name.
	 * 
	 * @param name
	 *            The entity's new name.
	 * @return this
	 */
	public Entity setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Sets the entity's new x coordinate without any regard for it's previous
	 * position.
	 * 
	 * @param x
	 *            The entity's new x coordinate.
	 * @return this
	 */
	public Entity setX(float x) {
		this.x = x;
		return this;
	}

	/**
	 * Sets the entity's new y coordinate without any regard for it's previous
	 * position.
	 * 
	 * @param y
	 *            The entity's new y coordinate.
	 * @return this
	 */
	public Entity setY(float y) {
		this.y = y;
		return this;
	}

	/**
	 * Subtracts damage from the entity's current health, then checks to see if
	 * the entity 'died.'
	 * 
	 * @param damage
	 *            How much damage the entity took.
	 */
	public void takeDamage(int damage) {
		maxHealth -= damage;
		if (maxHealth <= 0)
			this.die();
	}
	
	/**
	 * Update the entity. In inheriting sub classes, this method should be
	 * overridden, with the first call to super.update(int delta)
	 * 
	 * @param delta The time since the last update in milliseconds.
	 */
	public void update(int delta) {
		// TODO Should not regen in combat 
		if (health < maxHealth) {
			health = health + (int)(regen * delta);
			health = (health > maxHealth) ? maxHealth : health;
		}
	}
}