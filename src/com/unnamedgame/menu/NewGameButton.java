package com.unnamedgame.menu;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.state.NewGameState;

/**
 * 
 * @author Ryan Porterfield
 */
public class NewGameButton extends MenuButton {

	/**
	 * @param sizeH
	 * @param sizeV
	 * @param text
	 * @param x
	 * @param y
	 */
	public NewGameButton(int x, int y) {
		super("New Game", x, y, 200, 50);
	}

	/**
	 * @see com.unnamedgame.menu.StateChangeButton#onRelease()
	 */
	@Override
	public void onRelease() {
		UnnamedGame.GAME.enterState(NewGameState.ID);
	}
}
