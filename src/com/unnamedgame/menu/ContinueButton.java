package com.unnamedgame.menu;

import com.unnamedgame.UnnamedGame;
import com.unnamedgame.state.RunningState;

public class ContinueButton extends MenuButton {

	public ContinueButton(int x, int y) {
		super("Continue", x, y, 200, 50);
	}

	/**
	 * @see com.unnamedgame.menu.StateChangeButton#onRelease()
	 */
	@Override
	public void onRelease() {
		UnnamedGame.GAME.enterState(RunningState.ID);
	}
}
