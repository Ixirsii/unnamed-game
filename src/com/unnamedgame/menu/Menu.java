package com.unnamedgame.menu;

import java.util.Set;
import java.util.TreeSet;

import org.newdawn.slick.Graphics;

public class Menu {
	final private Set<MenuButton> buttons;

	public Menu() {
		buttons = new TreeSet<>();
	}

	public void addButton(MenuButton button) {
		buttons.add(button);
	}

	public void render(Graphics g) {
		for (MenuButton button : buttons) {
			button.draw(g);
		}
	}

	/**
	 * 
	 * @param x
	 *            Mouse x coordinate.
	 * @param y
	 *            Mouse y coordinate
	 * @return The button that was clicked, or null if no button was clicked.
	 */
	public MenuButton getButton(int x, int y) {
		for (MenuButton button : buttons) {
			if (button.isClicked(x, y))
				return button;
		}
		return null;
	}
}
