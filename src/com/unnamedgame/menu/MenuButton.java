package com.unnamedgame.menu;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.RoundedRectangle;
import org.newdawn.slick.geom.Shape;

public abstract class MenuButton implements Comparable<MenuButton> {
	boolean isPressed;
	/** The color to draw the button **/
	Color color;
	/** The shape of the button *******/
	Shape self;
	/** The text on the button ********/
	String text;
	private float textX;
	private float textY;

	/**
	 * Create a new Button with the default RoundedRectangle shape and light
	 * grey color.
	 * 
	 * @param text
	 *            What the button will say.
	 * @param x
	 *            The x coordinate of the button
	 * @param y
	 *            The y coordinate of the button
	 * @param width
	 *            The button's width
	 * @param height
	 *            The buttons' height
	 */
	MenuButton(String text, float x, float y, float width, float height) {
		this(text, new RoundedRectangle(x, y, width, height, 1F));
		
	}

	/**
	 * Creates a new Button with the specified shape and default light grey
	 * color.
	 * 
	 * @param text
	 *            What the button will say.
	 * @param shape
	 *            The shape of the button.
	 */
	MenuButton(String text, Shape shape) {
		this(text, shape, new Color(0.8F, 0.8F, 0.8F, 1F));
	}

	/**
	 * Creates a new Button with the specified shape and color.
	 * 
	 * @param text
	 *            What the button will say.
	 * @param shape
	 *            The shape of the button.
	 * @param color
	 *            The color of the button.
	 */
	MenuButton(String text, Shape shape, Color color) {
		this.isPressed = false;
		this.color = color;
		this.self = shape;
		this.text = text;
		textX = self.getCenterX() - ((text.length() / 2) * 9F);
		textY = self.getCenterY() - 7F;
	}
	
	@Override
	public int compareTo(MenuButton button) {
		return text.compareTo(button.text);
	}

	/**
	 * Draw the button to the screen.
	 * 
	 * @param g
	 *            Slick graphics drawing utility.
	 */
	public void draw(Graphics g) {
		g.setColor(color);
		g.fill(self);
		g.setColor(Color.white);
		g.drawString(text, textX, textY);
	}

	/**
	 * Based on the mouse coordinates, was this button clicked?
	 * 
	 * @param x
	 *            Mouse x coordinate
	 * @param y
	 *            Mouse y coordinate
	 * @return True if clicked, else False.
	 */
	public boolean isClicked(int x, int y) {
		return (x > self.getMinX() && x < self.getMaxX())
				&& (y > self.getMinY() && y < self.getMaxY());
	}
	
	/**
	 * Make the button color slightly darker
	 * 
	 * @param button
	 */
	public void onPress(int button) {
		if (button != 0)
			return;
		isPressed = true;
		color.r = 0.6F;
		color.g = 0.6F;
		color.b = 0.6F;
	}

	/**
	 * 
	 * @param button
	 */
	public void onRelease(int button) {
		if (button != 0)
			return;
		if (isPressed)
			onRelease();
		color.r = 0.8F;
		color.g = 0.8F;
		color.b = 0.8F;
		isPressed = false;
	}
	
	abstract void onRelease();
}
