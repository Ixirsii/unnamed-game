package com.unnamedgame.menu;



public class LoadButton extends MenuButton {

	public LoadButton(int x, int y) {
		super("Load", x, y, 200, 50);
	}
	
	/**
	 * @see com.unnamedgame.menu.StateChangeButton#onRelease()
	 */
	@Override
	public void onRelease() {
		// UnnamedGame.GAME.enterState(new LoadState(keyboard));
	}
}
